package az.com.mongodbintegration.entity;

import az.com.mongodbintegration.enums.Gender;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Document
public class Student {
    @Id
    String id;
    String firstName;
    String lastName;
//    @Indexed(unique = true)
    String email;
    Gender gender;
    Object address;
    List<String> favouriteSubjects;
    BigDecimal totalSpentInBooks;
    LocalDateTime create_date;


}
