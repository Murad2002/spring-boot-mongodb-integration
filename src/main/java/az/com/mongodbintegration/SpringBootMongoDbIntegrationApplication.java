package az.com.mongodbintegration;

import az.com.mongodbintegration.entity.Address;
import az.com.mongodbintegration.entity.Student;
import az.com.mongodbintegration.enums.Gender;
import az.com.mongodbintegration.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class SpringBootMongoDbIntegrationApplication implements CommandLineRunner {

    private final StudentRepository studentRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMongoDbIntegrationApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//        Address address = Address.builder()
//                .country("Azerbaijan")
//                .city("Baku")
//                .postCode("012")
//                .build();

        JSONObject jsonObject = new JSONObject("{\n" +
                "        country: 'Azerbaijan',\n" +
                "        city: 'Baku',\n" +
                "        postCode: '012'\n" +
                "    }");


        Student student = Student.builder()
                .firstName("Murad2")
                .lastName("Gadirov2")
                .address(jsonObject)
                .gender(Gender.MALE)
                .email("qadirovmurad2002@gmail.com")
                .create_date(LocalDateTime.now())
                .build();
//
//        studentRepository.insert(student);
//        log.info("Student : {}",studentRepository.findById("62f021c843d7dd67bd954862").get());
    }
}
